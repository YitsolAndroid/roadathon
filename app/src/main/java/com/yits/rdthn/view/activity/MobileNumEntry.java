package com.yits.rdthn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.yits.rdthn.R;

/**
 * Created by murali.hh on 05-Dec-17.
 */

public class MobileNumEntry extends AppCompatActivity {

    private EditText txt_MobileNum;
    private String mobilenum;
    private Button btn_Next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobilenum_entry);
        mobilenum=getIntent().getExtras().getString("mobilenum");
        txt_MobileNum = (EditText)findViewById(R.id.edt_mobilenum);
        btn_Next = (Button)findViewById(R.id.btn_next);
        txt_MobileNum.setText(mobilenum);

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileNumEntry.this, SentScreen.class);
                intent.putExtra("mobilenum", mobilenum);
                startActivity(intent);
            }
        });



    }



}
