package com.yits.rdthn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.yits.rdthn.R;

/**
 * Created by murali.hh on 04-Dec-17.
 */

public class ThridScreen extends AppCompatActivity{
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_screen);

        btnNext = (Button)findViewById(R.id.btn_next);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThridScreen.this,Registration.class);
                startActivity(intent);

            }
        });
    }
}
