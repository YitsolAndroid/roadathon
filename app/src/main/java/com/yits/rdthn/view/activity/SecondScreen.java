package com.yits.rdthn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.yits.rdthn.R;

public class SecondScreen extends AppCompatActivity {

    private Button btnTakeAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_screen);

        btnTakeAction = (Button)findViewById(R.id.btn_TakeAction);

        btnTakeAction.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent= new Intent(SecondScreen.this,ThridScreen.class);
                startActivity(intent);
            }


        });
    }
}
