package com.yits.rdthn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yits.rdthn.R;

/**
 * Created by murali.hh on 05-Dec-17.
 */

public class OTPScreen extends AppCompatActivity{


    private EditText edt_codedigit1;
    private EditText edt_codedigit2;
    private EditText edt_codedigit3;
    private EditText edt_codedigit4;
    private EditText edt_codedigit5;
    private EditText edt_codedigit6;
    private String mobilenum;
    private TextView txt_MobileNum;
    private Button btn_Continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);

        mobilenum=getIntent().getExtras().getString("mobilenum");

        edt_codedigit1 = (EditText)findViewById(R.id.code_digit1);
        edt_codedigit2 = (EditText)findViewById(R.id.code_digit2);
        edt_codedigit3 = (EditText)findViewById(R.id.code_digit3);
        edt_codedigit4 = (EditText)findViewById(R.id.code_digit4);
        edt_codedigit5 = (EditText)findViewById(R.id.code_digit5);
        edt_codedigit6 = (EditText)findViewById(R.id.code_digit6);
        txt_MobileNum = (TextView)findViewById(R.id.txt_mobilenum);
        btn_Continue = (Button)findViewById(R.id.btn_continue);

        txt_MobileNum.setText("+91 "+mobilenum);
        requestNextTextFocus(edt_codedigit1,edt_codedigit2);
        requestNextTextFocus(edt_codedigit2,edt_codedigit3);
        requestNextTextFocus(edt_codedigit3,edt_codedigit4);
        requestNextTextFocus(edt_codedigit4,edt_codedigit5);
        requestNextTextFocus(edt_codedigit5,edt_codedigit6);

        btn_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(OTPScreen.this,Dashboard.class);
                startActivity(intent);
            }
        });





    }

    private void requestNextTextFocus(final EditText edt1, final EditText edt2) {

        edt1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Integer textlength1 = edt1.getText().length();

                if (textlength1 >= 1) {
                    edt2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });

    }
}
