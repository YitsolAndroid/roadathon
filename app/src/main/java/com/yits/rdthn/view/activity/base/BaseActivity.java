package com.yits.rdthn.view.activity.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.yits.rdthn.R;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }
}
