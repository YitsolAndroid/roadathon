package com.yits.rdthn.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.yits.rdthn.R;

/**
 * Created by murali.hh on 05-Dec-17.
 */

public class Registration extends AppCompatActivity{


    private Button btn_Submit;
    private EditText edt_MobileNum;
    private String mobilenum;
    private char mDigit;
    private View btn_AlreadyRegistered;
    private PopupWindow pw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        btn_Submit = (Button)findViewById(R.id.btn_Submit);
        btn_AlreadyRegistered = (Button)findViewById(R.id.btn_AlreadyRegistered);
        edt_MobileNum= (EditText)findViewById(R.id.edt_mobilenum);

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobilenum = edt_MobileNum.getText().toString();

                if(checkMobileNumbervalidation(mobilenum)){
                Intent intent = new Intent(Registration.this,MobileNumEntry.class);
                intent.putExtra("mobilenum",mobilenum);
                startActivity(intent);
                }else{
                    Toast.makeText(Registration.this,"Please Provide Valid Mobile Number",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_AlreadyRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   initiatePopupWindow();
            }
        });


    }

    private void initiatePopupWindow() {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) Registration.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.mobilenum_popup, null);
            // create a 300px width and 470px height PopupWindow
            pw = new PopupWindow(layout, 700, 800, true);
            // display the popup in the center
        //    pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

            pw.setOutsideTouchable(true);
            pw.setFocusable(true);
            // Removes default background.
            pw.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pw.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        pw.dismiss();
                    }
                    return false;
                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkMobileNumbervalidation(String cust_regmobile) {

        if (cust_regmobile.length() > 0) {
            mDigit = cust_regmobile.charAt(0);

            if (cust_regmobile.length() == 10 && (mDigit=='9' || mDigit=='8' || mDigit=='7')) {
                    return true;
            } else {
                return false;

            }
        }
        return false;
    }

}
