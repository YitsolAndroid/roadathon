package com.yits.rdthn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.yits.rdthn.R;

import java.util.Timer;
import java.util.TimerTask;

public class SentScreen extends AppCompatActivity {


    Timer timer;
    private String mobilenum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_sent);
        mobilenum = getIntent().getExtras().getString("mobilenum");
        timer = new Timer();


    }


    @Override
    public void onResume() {
        super.onResume();
        System.gc();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(SentScreen.this, OTPScreen.class);
                intent.putExtra("mobilenum", mobilenum);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    protected void onPause() {
        super.onPause();
    }


}

